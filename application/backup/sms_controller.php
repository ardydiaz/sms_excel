<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sms extends CI_Controller
{

	private $response = array(
		'success' => '',
		'message' => '',
		'errorCode' => '',
		'data' => array()
	);
	public function __construct()
	{
		Parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->database();
		$this->load->library('PHPexcel');
		$this->load->library('excel');
		/*model*/
		$this->load->model('sms_model');
		$this->load->model('user_model');
		$this->load->model('search_member_model');
		if ((!$this->session->userdata('islogin'))) {
			redirect(site_url() . "auth", 'refresh');
		}
	}

	private function sendResponse()
	{
		if ($this->response['errorCode']) {
			header('HTTP/1.1 ' . $this->response['errorCode'] . ' Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
		} else {
			$this->response['success'] = true;
		}
		echo json_encode($this->response);
	}

	public function index()
	{
		$data['title'] = "Excel";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/sms', $data, TRUE);
		$this->load->view('layout', $data);
	}

	public function import()
	{
		$data['title'] = "File Upload";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/import', $data, TRUE);
		$this->load->view('layout', $data);
	}

	public function saveImportdd()
	{
		$allowedtypes = array('application/vnd.ms-excel', 'text/xls', 'text/xlsx', 'text/csv', 'application/csv', 'application/excel');
		if (in_array($_FILES['file']['type'], $allowedtypes)) {
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach ($object->getWorksheetIterator() as $worksheet) {
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for ($row = 2; $row <= $highestRow; $row++) {
					$group = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$merchant_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					$member_group = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					$login_id = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					$player_id = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$currency = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
					$transfer_in = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
					$trans_in_amt = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
					$transfer_out = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
					$trans_out_amt = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
					$adjustment = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
					$adjustment_amt = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
					$bets = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
					$bets_amount = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
					$bets_valid_amt = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
					$bets_win_loss = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
					$bets_comm = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
					$bets_bonus_amt = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
					$bets_adj = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
					$bets_adj_amt = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
					$bets_total_win_loss = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
					$bets_jackpot = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

					$data = array(
						'group' => $group,
						'merchant_id' => $merchant_id,
						'member_group' => $member_group,
						'login_id' => $login_id,
						'player_id' => $player_id,
						'currency' => $currency,
						'transfer_in' => $transfer_in,
						'trans_in_amt' => $trans_in_amt,
						'transfer_out' => $transfer_out,
						'adjustment' => $adjustment,
						'adjustment_amt' => $adjustment_amt,
						'bets' => $bets,
						'bets_amount' => $bets_amount,
						'bets_valid_amt' => $bets_valid_amt,
						'bets_win_loss' => $bets_win_loss,
						'bets_comm' => $bets_comm,
						'bets_bonus_amt' => $bets_bonus_amt,
						'bets_adj' => $bets_adj,
						'bets_adj_amt' => $bets_adj_amt,
						'bets_total_win_loss' => $bets_total_win_loss,
						'bets_jackpot' => $bets_jackpot
					);


					$this->db->insert('excel_files', $data);
				}
			}
		} else {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			echo 'Please upload a legal file format !';
		}
	}



	public function saveImport()
	{

		if (isset($_FILES['file'])) {
			$file = $_FILES['file'];
			$allowedtypes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/xlsx', 'text/tsv', 'application/csv', 'text/comma-separated-values', 'application/excel');
			if (in_array($_FILES["file"]["type"], $allowedtypes)) {
				if ($file['type'] == "text/csv") { //USE EXCEL READER IF FILE IS EXCEL
					$path = $_FILES["file"]["tmp_name"];
					$object = PHPExcel_IOFactory::load($path);
					foreach ($object->getWorksheetIterator() as $worksheet) {
						$highestRow = $worksheet->getHighestRow();
						$highestColumn = $worksheet->getHighestColumn();
						for ($row = 2; $row <= $highestRow; $row++) {
							$group = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$merchant_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
							$member_group = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$login_id = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							$player_id = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
							$currency = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
							$transfer_in = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
							$trans_in_amt = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
							$transfer_out = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
							$trans_out_amt = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
							$adjustment = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
							$adjustment_amt = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
							$bets = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
							$bets_amount = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
							$bets_valid_amt = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
							$bets_win_loss = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
							$bets_comm = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
							$bets_bonus_amt = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
							$bets_adj = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
							$bets_adj_amt = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
							$bets_total_win_loss = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
							$bets_jackpot = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

							$checkMember = $this->getMember($login_id, $member_group, $merchant_id);
						     echo '<pre>';
							 print_r($checkMember);
							// die;
							if(empty($checkMember)){
								$data = array(
									'name' => $this->removeSpecialChar($login_id),
									'class' => $this->removeSpecialChar($member_group),
									'group_id' => $this->removeSpecialChar($merchant_id),
								);
								$this->db->insert('member', $data);
								$recordId = $this->db->insert_id();
								$data2 = array(
									'group' => $this->removeSpecialChar($group),
									'merchant_id' => $this->removeSpecialChar($merchant_id),
									'member_group' => $this->removeSpecialChar($member_group),
									'member_id' => $recordId,
									'login_id' => $this->removeSpecialChar($login_id),
									'player_id' => $this->removeSpecialChar($player_id),
									'currency' => $this->removeSpecialChar($currency),
									'transfer_in' => $transfer_in,
									'trans_in_amt' => $trans_in_amt,
									'transfer_out' => $transfer_out,
									'trans_out_amt' => $trans_out_amt,
									'adjustment' => $adjustment,
									'adjustment_amt' => $adjustment_amt,
									'bets' => $bets,
									'bets_amount' => $bets_amount,
									'bets_valid_amt' => $bets_valid_amt,
									'bets_win_loss' => $bets_win_loss,
									'bets_comm' => $bets_comm,
									'bets_bonus_amt' => $bets_bonus_amt,
									'bets_adj' => $bets_adj,
									'bets_adj_amt' => $bets_adj_amt,
									'bets_total_win_loss' => $bets_total_win_loss,
									'bets_jackpot' => $bets_jackpot,
									'created_at' => date('Y-m-d'),
								);
								$this->db->insert('excel_files', $data2);
							}else{
								// $recordId = $this->db->insert_id();
								// $data3 = array(
								// 	'group' => $this->removeSpecialChar($group),
								// 	'merchant_id' => $this->removeSpecialChar($merchant_id),
								// 	'member_group' => $this->removeSpecialChar($member_group),
								// 	'member_id' => $recordId,
								// 	'login_id' => $this->removeSpecialChar($login_id),
								// 	'player_id' => $this->removeSpecialChar($player_id),
								// 	'currency' => $this->removeSpecialChar($currency),
								// 	'transfer_in' => $transfer_in,
								// 	'trans_in_amt' => $trans_in_amt,
								// 	'transfer_out' => $transfer_out,
								// 	'trans_out_amt' => $trans_out_amt,
								// 	'adjustment' => $adjustment,
								// 	'adjustment_amt' => $adjustment_amt,
								// 	'bets' => $bets,
								// 	'bets_amount' => $bets_amount,
								// 	'bets_valid_amt' => $bets_valid_amt,
								// 	'bets_win_loss' => $bets_win_loss,
								// 	'bets_comm' => $bets_comm,
								// 	'bets_bonus_amt' => $bets_bonus_amt,
								// 	'bets_adj' => $bets_adj,
								// 	'bets_adj_amt' => $bets_adj_amt,
								// 	'bets_total_win_loss' => $bets_total_win_loss,
								// 	'bets_jackpot' => $bets_jackpot,
								// 	'created_at' => date('Y-m-d'),
								// );
								// $this->db->insert('excel_files', $data2);
							}

						}
					}
				}
			} else {
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				echo 'Please upload a legal file format !';
			}
		}
	}


	public function getMember($login_id, $member_group, $merchant_id){
		$array = array();
		$data = array(
			'name' => $this->removeSpecialChar($login_id),
			'class' => $this->removeSpecialChar($member_group),
			'group_id' => $this->removeSpecialChar($merchant_id),
		);
		$this->db->select('*')->from('member')->where($data);
		$get = $this->db->get();

		if(!empty($get)){
              foreach($get->result() as $row){
                   $array[] = $row->name;
				   $array[] = $row->class;
				   $array[] = $row->group_id;
			  }
		}
		return $array;
	}



    public function getExcelResult(){
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$query = $this->sms_model->showExcel($from, $to);
        $result = '';
		$data = array();
		foreach($query as $values){
			if(!isset($data[$values['created_at']])) {
				$data[$values['created_at']] = $values;
				// echo '<pre>';
		        // print_r($data);
			} else {
				// $data[$values['created_at']]['login_id'] .= ', ' . $values['login_id'];
				// $data[$values['created_at']]['player_id'] .= ', ' . $values['player_id'];
			}
		}
		krsort($data);
		if(sizeof($data) == 0){
			echo '<h3 class="text-danger">No result !</h3>';
		}else{
			$result .=' <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">';
			foreach($data as $item){
				$result .=' <div class="btn-group mr-2 mrgin-bot">
					<a href="javascript:void(0);" id="get_excel" class="btn btn-default grey" data-date='.$item['created_at'].' onclick="list_methods.view_result(this)">'.$item['created_at'].'</a></div>';
			}
				$result .= '
					</div>
			</div>';
		}

		 echo $result;
		
	}
	public function showExcelDate()
	{
		$date = $this->input->post('date');
		if (isset($date) and !empty($date)) {
			$result = $this->sms_model->getExcel($date);
            $html = '';
			$html .='<table id="excel_data" class="table table-bordered table-striped table-custom">
						<thead>
							<tr>
								<th>No.</th>
								<th>Group </th>
								<th>MerchantID</th>
								<th>Member Group</th>
								<th>Login ID</th>
								<th>Player ID</th>
								<th>Currency</th>
								<th>Transfer-#In</th>
								<th>Transfer-In Amt</th>
								<th>Transfer-#Out</th>
								<th>Transfer-Out Amt</th>
								<th>Adjustment-#Adj</th>
								<th>Adjustment-Adj Amt</th>
								<th>Bets-#Bets</th>
								<th>Bets-Bet Amt</th>
								<th>Bets-Valid Bet Amt</th>
								<th>Bets-Win/Loss</th>
								<th>Bets-Comm</th>
								<th>Bets-BonusAmt</th>
								<th>Bets-#Adj</th>
								<th>Bets-Adj Amt</th>
								<th>Bets-Total W/L</th>
								<th>Bets Jackpot</th>
								<th>Date Uploaded</th>
							</tr>
						</thead>';
						$no = 1;
						foreach($result as $item){

							$html .= '
							        <tr>
									      <td>'.$no.'</td>
                                          <td>'.$item['group'].'</td>
										  <td>'.$item['merchant_id'].'</td>
										  <td>'.$item['member_group'].'</td>
										  <td>'.$item['memberName'].'</td>
										  <td>'.$item['login_id'].'</td>
										  <td>'.$item['player_id'].'</td>
										  <td>'.$item['currency'].'</td>
										  <td>'.$item['transfer_in'].'</td>
										  <td>'.$item['trans_in_amt'].'</td>
										  <td>'.$item['transfer_out'].'</td>
										  <td>'.$item['trans_out_amt'].'</td>
										  <td>'.$item['adjustment'].'</td>
										  <td>'.$item['adjustment_amt'].'</td>
										  <td>'.$item['bets_amount'].'</td>
										  <td class="bg-danger text-white">'.$item['bets_valid_amt'].'</td>
										  <td>'.$item['bets_win_loss'].'</td>
										  <td>'.$item['bets_comm'].'</td>
										  <td>'.$item['bets_bonus_amt'].'</td>
										  <td>'.$item['bets_adj'].'</td>
										  <td>'.$item['bets_adj_amt'].'</td>
										  <td>'.$item['bets_total_win_loss'].'</td>
										  <td>'.$item['bets_jackpot'].'</td>
										  <td>'.$item['created_at'].'</td>
									</tr>
							
							';
							$no++;
						}

			$html .='</table>
			<script>
			   $(document).ready(function(){
				$("#excel_data").DataTable({
					"lengthMenu": [
						[30, 60, 100, 200],
						[30, 60, 500, "All"]
					],
				});
			   });
			</script>
			<div class="loading3" style="display: none;">
			   <div class="content"><img src=' . base_url() . 'assets/img/loading.gif' . ' /></div>
			</div>';
            echo $html;
		}
		
	}
    public function searchMember(){
		$data['title'] = "Search by Member";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/search_member', $data, TRUE);
		$this->load->view('layout', $data);
	}


	public function memberResult()
	{
		$list = $this->search_member_model->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $item) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $item->group;
			$row[] = $item->merchant_id;
			$row[] = $item->member_group;
			$row[] = $item->name;
			$row[] = $item->player_id;
			$row[] = $item->currency;
			$row[] = $item->transfer_in;
			$row[] = $item->trans_in_amt;
			$row[] = $item->transfer_out;
			$row[] = $item->trans_out_amt;
			$row[] = $item->adjustment;
			$row[] = $item->adjustment_amt;
			$row[] = $item->bets;
			$row[] = $item->bets_amount;
			$row[] = $item->bets_valid_amt ? '<div class="bg-danger text-center">' . $item->bets_valid_amt . '</div>' : '';
			$row[] = $item->bets_win_loss;
			$row[] = $item->bets_comm;
			$row[] = $item->bets_bonus_amt;
			$row[] = $item->bets_adj;
			$row[] = $item->bets_adj_amt;
			$row[] = $item->bets_total_win_loss;
			$row[] = $item->bets_jackpot;
			$row[] = $item->created_at;
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->search_member_model->count_all(),
			"recordsFiltered" => $this->search_member_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}


	public function removeSpecialChar($str)
	{
		$res = preg_replace('/[\@\=\.\;\" "]+/', '', $str);
		return $res;
	}
	
}
