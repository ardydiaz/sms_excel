<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script src="<?php echo base_url(); ?>plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<script src="<?php echo base_url(); ?>assets/sweetalert/js/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url(); ?>assets/sweetalert/js/sweetalert2.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>plugins/moment/moment.min.js"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(); ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(); ?>plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>dist/js/demo.js"></script>
<script src="<?php echo base_url(); ?>assets/js/alert.js"></script>


<!-- date range -->
<script src="<?php echo base_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.1.2/js/dataTables.dateTime.min.js">
<script>
    $(function() {
        //Date and time picker
        // $('#from_date').datetimepicker({ 
        //     icons: { 
        //     time: 'far fa-clock' 
        // } });
        // $('#to_date').datetimepicker({ icons: { time: 'far fa-clock' } });
        bsCustomFileInput.init();
        $('#from_date').datetimepicker({
            //format: 'L'
            format: 'YYYY-MM-DD'
        });
        $('#to_date').datetimepicker({
            format: 'YYYY-MM-DD'
        });

        $(this).bind("contextmenu", function(e) {
            e.preventDefault();
        });
    });
</script>
</body>

</html>