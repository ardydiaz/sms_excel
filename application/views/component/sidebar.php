<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?php echo base_url('user/') ?>" class="brand-link">
    <img src="<?php echo base_url() ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Excel Management</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo base_url() ?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="<?php echo site_url('user/'); ?>" class="d-block"><?php echo $username['username']; ?></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

        <?php if ($role == Admin) { ?>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                User Management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview menu-open">
              <li class="nav-item ">
                <a href="<?php echo site_url('user/'); ?>" class="<?php if (in_array($this->uri->uri_string(), array('user', 'user/index'))) echo 'active'; ?> nav-link ">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Admin</p>
                </a>
              </li>
            </ul>
          </li>

        <?php }
        if ($role == Normal || $role == Admin) { ?>
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Excel management
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo site_url('sms/import/'); ?>" class="nav-link <?php if ($this->uri->uri_string() == 'sms/import') echo 'active'; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Upload file</p>
                </a>
              </li>
            </ul>
          </li>
        <?php }
        if ($role == Normal || $role == Admin) { ?>
          <li class="nav-item">
            <a href="<?php echo base_url('sms/searchMember'); ?>" class="nav-link <?php if ($this->uri->uri_string() == 'sms/searchMember') echo 'active'; ?>">
              <i class="nav-icon far fa-user"></i>
              <p>Search By Member</p>
            </a>
          </li>
        <?php } ?>





      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>