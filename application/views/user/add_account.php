<div class="row">
  <div class="col-4">
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Create account</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <div class="form">
          <!-- text input -->
          <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control" id="username" placeholder="username ...">
          </div>
         
          <div class="form-group">
            <label>Fullname</label>
            <input type="text" class="form-control" id="fullname" placeholder="fullname ...">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" id="password" placeholder="password ...">
          </div>
          <div class="form-group">
            <label>Role</label>
            <select name="role" id="role" class="form-control">
              <option value="">Select role</option>
              <?php
                  $role = $this->db->query('SELECT * FROM role')->result();
                  foreach ($role as $roles) { ?>
                  <option value="<?php echo $roles->id; ?>"><?php echo $roles->name; ?></option>
              <?php } ?>
              
            </select>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary" onclick="methods.addAdmin();">Submit</button>
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->
  </div>
  <div class="col-8">
    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">List Account</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <table id="users" class="table table-bordered table-hover">
          <thead>
            <tr>
              <th>Fullname</th>
              <th>Username</th>
              <th>Role</th>
              <th>Date Created</th>
            </tr>
          </thead>

        </table>
      </div>
      <!-- /.card-body -->

    </div>
    <!-- /.card -->
  </div>
</div>
<script>
  var listTable = null;
  $(document).ready(function() {
    listTable = $('#users').DataTable({
      " processing": true,
      "serverSide": true,
      "searching": true,
      "lengthMenu": [
        [10, 25, 50, 100],
        [10, 25, 100, "All"]
      ],
      "order": [
        [3, "desc"]
      ],

      "language": {
        "processing": "Processing...",
        "loadingRecords": "Loading...",
        "lengthMenu": "Show _MENU_ entries",
        "zeroRecords": "No matching records found",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "infoEmpty": "Showing 0 to 0 of 0 entries",
        "infoFiltered": "(filtered from _MAX_ total entries)",
        "infoPostFix": "",
        "search": "Search:",
        "paginate": {
          "first": "First",
          "last": "Last",
          "next": "Next",
          "previous": "Previous"
        },
        "aria": {
          "sortAscending": ": Ascending",
          "sortDescending": ": Descending"
        }
      },
      "ajax": {
        "url": "<?php echo base_url(); ?>" + 'user/getUsers',
      },
    });
  });
</script>