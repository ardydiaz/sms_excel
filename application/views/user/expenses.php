<style>
    #log {
        font-size: .9rem;
        font-weight: 400;
        line-height: .8;
    }
</style>
<div class="card">
    <div class="card-header">
        <h3 class="card-title"><?php echo $title; ?></h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="log" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>内容</th>
                    <th>条数</th>
                    <th>每条费用</th>
                    <th>总费用</th>
                    <th>操作前金额</th>
                    <th>操作后余额</th>
                    <th>状态</th>
                    <th>笔记</th>
                    <th>时间</th>
                </tr>
            </thead>

        </table>
    </div>
    <!-- /.card-body -->
</div>
</div>
<script>
    $(document).ready(function() {
        $("#log").DataTable({
            "processing": true,
            "serverSide": true,
            "lengthMenu": [50, 100, 150, 250],
            "order": [
                [8, "DESC"]
            ],
            "columnDefs": [{
                    "render": function(data, type, row) {
                        return data == '0' ? '待发送' : (data == '1' ? '成功' : (data == '2' ? '失败' : '余额更新'));
                    },
                    "targets": 6
                },
                {
                    "render": function(data, type, row) {
                        $text = data.content ? data.content : '（未指定内容名称）';
                        return '<a href="' + data.download + '" download />  ' + $text + ' </a> ';
                    },
                    "targets": 0
                }
            ],
            "language": {
                "processing": "处理中...",
                "loadingRecords": "载入中...",
                "lengthMenu": "显示 _MENU_ 页结果",
                "zeroRecords": "沒有符合的结果",
                "info": "显示第 _START_ 至 _END_ 页结果，共 _TOTAL_ 页",
                "infoEmpty": "显示第 0 至 0 页结果，共 0 页",
                "infoFiltered": "(从 _MAX_ 页结果中过滤)",
                "infoPostFix": "",
                "search": "搜索:",
                "paginate": {
                    "first": "第一页",
                    "previous": "上一页",
                    "next": "下一页",
                    "last": "最后一页"
                },
                "aria": {
                    "sortAscending": ": 升序排列",
                    "sortDescending": ": 降序排列"
                }
            },
            "ajax": {
                "url": "<?php echo base_url(); ?>" + 'User/getLogs',
            }
        });
    });
</script>