<div class="row">
    <div class="col-5">

        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle" src="<?php echo base_url(); ?>dist/img/profile.png" alt="User profile picture">
                </div>
                <h3 class="profile-username text-center"> <?php echo $username ?></h3>
                <h3><?php echo $title; ?></h3><br>
                <?php echo form_open('user/change_password/', array('id' => 'passwordForm')) ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>成功!</strong> <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } else if ($this->session->flashdata('error')) {  ?>

                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        <strong>失败!</strong> <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php echo validation_errors("<p class='text-danger'>", "</p>"); ?>
                <div class="form-group">
                    <label for="">旧密码</label>
                    <input type="password" name="oldpass" class="form-control" placeholder="旧密码">
                </div>
                <div class="form-group">
                    <label for="">新密码</label>
                    <input type="password" name="newpass" class="form-control" placeholder="新密码">
                </div>
                <div class="form-group">
                    <label for="">确认密码</label>
                    <input type="password" name="passconf" class="form-control" placeholder="确认密码">
                </div>
                <button type="submit" class="btn btn-primary">修改密码</button>
                <?php echo form_close(); ?>
            </div>
            <!-- /.card-body -->
        </div>

    </div>
</div>