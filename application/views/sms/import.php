<style>
    .col-sm-12 {
        overflow-x: auto;
    }
</style>
<div id="drophere" class="card">
    <div class="card-header">
        <h3 class="card-title">Format : CSV / XLSX</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
        </div>
    </div>



    <div id="actions" class="row">
        <div class="card-body">
            <div class="row" id="form-filter">
                <div class="col-lg-6">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <div id="dragarea" class=" fileinput-button dz-clickable" style="border: 1px dashed;border-width: 3px;border-color: #636567;height: 150px;text-align: center;padding: 50px;">
                        Drag the file here or click in the area.
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>From:</label>
                        <div class="input-group date" id="from_date" data-target-input="nearest">
                            <input type="text" id="search_fromdate" name="from" class="form-control datetimepicker-input" data-target="#from_date" placeholder="YYYY-MM-DD">
                            <div class="input-group-append" data-target="#from_date" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>From:</label>
                        <div class="input-group date" id="to_date" data-target-input="nearest">
                            <input type="text" id="search_todate" name="to" class="form-control datetimepicker-input" data-target="#to_date" placeholder="YYYY-MM-DD">
                            <div class="input-group-append" data-target="#to_date" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-2">
                    <div class="btn-search">
                        <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                        <button type="button" id="btn-reset" class="btn btn-danger">Reset</button>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <!-- The global file processing state -->
                <span style="display:none" class="fileupload-process">
                    <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="opacity: 0;">
                        <div class="progress-bar progress-bar-success" style="width: 100%;" data-dz-uploadprogress=""></div>
                    </div>
                </span>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-7">
            <div class="card-body">
                <div class="table table-striped" class="files" id="previews">
                    <div id="template" class="file-row">
                        <!-- This is used as the file preview template -->
                        <div>
                            <span class="preview"><img data-dz-thumbnail /></span>
                        </div>

                        <div>
                            <p class="name" data-dz-name></p>
                            <strong class="error text-danger" data-dz-errormessage></strong>
                        </div>
                        <div>
                            <p class="size" data-dz-size></p>
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                            </div>
                        </div>
                        <div class="options-actions">
                            <button class="btn btn-primary start">
                                <i class="glyphicon glyphicon-upload"></i>
                                <span>Upload</span>
                            </button>
                            <button data-dz-remove class="btn btn-warning cancel">
                                <i class="glyphicon glyphicon-ban-circle"></i>
                                <span>Cancel</span>
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card-body">
        <div class="btn-toolbar" id="result_details"></div>
    </div>
    <div class="card-body">
        <div id="excel_resultss"></div>

    </div>

</div>
<div class="loading" style="display: none;">
    <div class="content"><img src="<?php echo base_url() . 'assets/img/spinner-icon-gif-1.jpg'; ?>" /></div>
</div>
<div class="loading3" style="display: none;">
    <div class="content"><img src="<?php echo base_url() . 'assets/img/loading.gif'; ?>" /></div>
</div>
<div class="loading4" style="display: none;">
    <div class="content"><img src="<?php echo base_url() . 'assets/img/loading.gif'; ?>" /></div>
</div>
<script>
    var listTable = null;
    var dateExcel = null;
    $(document).ready(function() {
        bsCustomFileInput.init();
        $('#excel_data').parents('div.card-body').first().hide();
        $('body').on("keyup", "textarea.filedescription", function() {
            let trimval = ($(this).val()).substring(0, 60);
            $(this).val(trimval);
            $(".counter .char-count").html((60 - $(this).val().length));
        });

        // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzone = new Dropzone('#drophere', {
            url: "<?php echo base_url(); ?>sms/saveImport", // Set the url
            maxFiles: 100000,
            maxFilesize: 1000, // MB
            timeout: 3000000,
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            clickable: ".dz-clickable" // Define the element that should be used as click trigger to select files.
        });

        myDropzone.on("addedfile", function(file) {
            // Hookup the start button
            file.previewElement.querySelector(".start").onclick = function() {
                myDropzone.enqueueFile(file);
            };
        });

        // // Update the total progress bar
        myDropzone.on("totaluploadprogress", function(progress) {
            document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
        });

        myDropzone.on("sending", function(file, xhr, formData) {
            //ADDITIONAL DATA
            $description = $(".filedescription").val();
            formData.append('content', $description);
            // Show the total progress bar when upload starts
            document.querySelector("#total-progress").style.opacity = "1";
            // And disable the start button
            file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
            $(file.previewElement).find(".start").html('uploading...')
            $('.loading').show();

        });

        myDropzone.on("error", function(file, errormessage, xhr) {
            if (xhr) {
                //toastr.error(xhr.responseText);
                Swal.fire({
                    icon: 'error',
                    title: "" + xhr.responseText + "",
                    showConfirmButton: false,
                    //timer: 2200
                })
                $('.loading').hide();
                $(".loading").css("display", "none");
                //location.reload();
                $(file.previewElement).find(".start").html('error').removeClass('btn-primary').addClass('btn-danger');
                myDropzone.files = [];
            }
        });

        myDropzone.on("maxfilesexceeded", function(file) {
            toastr.error('Exceed the maximum number of files, only one file is allowed !');
            myDropzone.removeFile(file);
            location.reload();
        });

        // Hide the total progress bar when nothing's uploading anymore
        myDropzone.on("queuecomplete", function(progress) {
            document.querySelector("#total-progress").style.opacity = "0";

        });

        myDropzone.on("success", function(file, errormessage, xhr) {
            console.log(errormessage);
            if (xhr) {
                Swal.fire({
                    icon: 'success',
                    title: "Data Imported successfully!",
                    showConfirmButton: false,
                    timer: 2200
                })
                $('.start').html('Import Done');
                $('.loading').hide();
                listTable.ajax.reload();
                myDropzone.files = [];
            }
        });

        // myDropzone.on("success", function(data) {
        //     var parsedData = JSON.parse(data.xhr.response);
        //     var uploadElement = data.previewElement;

        //     Swal.fire({
        //         icon: "success",
        //         title: "Submitted successfully",
        //         showConfirmButton: false,
        //     });
        //     listTable.ajax.reload();
        //     $(uploadElement).find('.file-succes').fadeIn();
        //     $(data.previewElement).find(".start").html('done').removeClass('btn-primary').addClass('btn-success');
        //     $(data.previewElement).find('.progress-bar-success').css('background', '#28a745');
        //     myDropzone.files = [];
        // });


        $('#import_form').on('submit', function(e) {
            e.preventDefault();

            $.ajax({
                url: "<?php echo base_url(); ?>sms/saveImport",
                method: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
            }).then(function(data, res, xhr) {
                    console.log(data);
                    $('.start').html('Importing...');
                    if (xhr.status == 200) {
                        Swal.fire({
                            icon: "success",
                            title: "Data Imported successfully!",
                            showConfirmButton: false,
                            timer: 2200
                        });
                        $('.start').html('Import Done');
                        listTable.ajax.reload();


                    }
                },
                function(data) {
                    Swal.fire({
                        icon: "error",
                        title: "" + data.responseJSON.message + "",
                        showConfirmButton: false,
                        timer: 2200
                    });
                    $('.loading').hide();
                    $(".loading").css("display", "none");
                    listTable.ajax.reload();
                })

        });



        $('#btn-filter').click(function() { //button filter event click
            var from = $("#search_fromdate").val();
            var to = $("#search_todate").val();
            $('.btn-toolbar').show();
            if (from != '' || to != '') {
                $.ajax({
                    url: base_url + '/sms/getExcelResult/',
                    method: "POST",
                    dataType: 'json',
                    data: {
                        from: from,
                        to: to
                    },
                    success: function(data) {
                        $("#search_fromdate").val('');
                        $("#search_todate").val('');
                        for (var key in data) {
                            for (var i = 0; i < data[key].length; i++) {
                                var date = data[key][i].created_at;
                                var badge = document.createElement('div');
                                badge.className = 'btn-group mr-2 mrgin-bot';
                                badge.innerHTML =
                                    '<a href="<?php echo base_url() ?>sms/showExcelDate/' + date + '" class="btn btn-default grey" data-date=' + date + ' target="_blank">' + date + '</a>';
                                document.getElementById('result_details').append(badge);
                            }
                        }

                    },
                    error: function(data) {
                        $("#search_fromdate").val('');
                        $("#search_todate").val('');
                        Swal.fire({
                            icon: 'error',
                            title: "" + data.responseText + "",
                            showConfirmButton: false,
                            //timer: 2200
                        });
                    }
                });

                $('#excel_data').parents('div.card-body').first().show();
            } else {
                Swal.fire({
                    icon: "error",
                    title: "Please select both date field !",
                    showConfirmButton: false,
                    timer: 2200
                });
            }

        });
        $('#btn-reset').click(function() { //button reset event click
            location.reload();
            $("#search_fromdate").val('');
            $("#search_todate").val('');
            $('#excel_data').parents('div.card-body').first().hide();
        });

        $('#dragarea').click(function() {
            $('#excel_result').hide();
        });
        $('.input-group-text').click(function() {
            $('.btn-toolbar').hide();
            $('#result_details').html('');
        });




    });
</script>