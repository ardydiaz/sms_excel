<style>
    .col-sm-12 {
        overflow-x: auto;
    }

    .custom {
        display: contents ! important;
    }

    table.datatable {
        width: 100%;
        border: none;
        background: #fff;
    }

    table.datatable td.table_foot {
        border: none;
        background: #fff;
        text-align: center;
    }

    table.datatable tr.odd_col {
        background: none;
    }

    table.datatable tr.even_col {
        background: #ddd;
    }

    table.datatable td {
        font-size: 12px;
        padding: 1px 0px;
        border-bottom: 1px solid #ddd;
        text-align: center;
    }

    table.datatable th {
        text-align: center;
        font-size: 8pt;
        padding: 8px 7px 8px;
        color: #fff;
        background: #000;
        font-family: sans-serif;
        cursor: pointer;
    }

    table.datatable th a {
        color: #fff;
    }

    table.datatable th.sort_ASC:after {
        content: "▲";
        color: #ffc107;
    }

    table.datatable th.sort_DESC:after {
        content: "▼";
        color: #ffc107;
    }
</style>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Page : Search By Member</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fas fa-times"></i></button>
        </div>
    </div>

    <div class="card-body">

        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Search Member:</label>
                    <input type="text" class="form-control" id="member" name="member" placeholder="Enter member...">
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>From:</label>
                    <div class="input-group date" id="from_date" data-target-input="nearest">
                        <input type="text" id="search_fromdate" name="from" class="form-control datetimepicker-input" data-target="#from_date" placeholder="YYYY-MM-DD">
                        <div class="input-group-append" data-target="#from_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <label>From:</label>
                    <div class="input-group date" id="to_date" data-target-input="nearest">
                        <input type="text" id="search_todate" name="to" class="form-control datetimepicker-input" data-target="#to_date" placeholder="YYYY-MM-DD">
                        <div class="input-group-append" data-target="#to_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="btn-search">
                    <button type="button" id="btn-filter" class="btn btn-primary" onclick="sendRequest();">Search</button>
                    <a href="<?php echo base_url('sms/searchMember') ?>" id="btn-reset" class="btn btn-danger">Reset</a>
                </div>
            </div>


        </div>
        <br>
        <div class="row">
            <div class="form-group col-sm-1">
                <label for="">Show:</label>
                <select class="form-control select-sort" id="limitRows" onchange="sendRequest();">
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="80">80</option>
                    <option value="100">100</option>
                    <option value="200">200</option>
                    <option value="500">500</option>

                </select>
            </div>
        </div>
        <?php
        if ($item_list) {
        ?>
            <table id="member_table" class="datatable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th> Group <i class="fa fa-sort-amount-asc fa-sort"></i> </th>
                        <th data-action="sort" data-title="merchant_id" data-direction="ASC"> MerchantID <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="member_group" data-direction="ASC"> Member Group <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="member_id" data-direction="ASC"> Member Name <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="player_id" data-direction="ASC">Player ID <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="currency" data-direction="ASC">Currency <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="transfer_in" data-direction="ASC">Transfer-In <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="trans_in_amt" data-direction="ASC">Transfer-In Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="transfer_out" data-direction="ASC">Transfer-Out <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="trans_out_amt" data-direction="ASC">Transfer-Out Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="adjustment" data-direction="ASC">Adjustment <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="adjustment_amt" data-direction="ASC">Adjustment Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets" data-direction="ASC"> Bets <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_amount" data-direction="ASC">Bets-Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_valid_amt" data-direction="ASC">Bets-Valid Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_win_loss" data-direction="ASC">Bets-Win/Loss <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_comm" data-direction="ASC">Bets-Comm <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_bonus_amt" data-direction="ASC">Bets-Bonus-Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_adj" data-direction="ASC">Bets-Adj <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_adj_amt" data-direction="ASC">Bets-Adj Amt <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_total_win_loss" data-direction="ASC">Bets-Total W/L <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="bets_jackpot" data-direction="ASC">Bets Jackpot <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                        <th data-action="sort" data-title="date_upload" data-direction="ASC">Date Upload <i class="fa fa-sort-amount-asc fa-sort"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    $no = ($this->uri->segment(3)) ? (($this->uri->segment(3) - 1) * 10) + 1 : 1;
                    foreach ($item_list as $item) {
                    ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $item->group; ?></td>
                            <td><?php echo $item->merchant_id; ?></td>
                            <td><?php echo $item->member_group; ?></td>
                            <td><?php echo $item->memberName; ?></td>
                            <td><?php echo $item->player_id; ?></td>
                            <td><?php echo $item->currency; ?></td>
                            <td><?php echo $item->transfer_in; ?></td>
                            <td><?php echo $item->trans_in_amt; ?></td>
                            <td><?php echo $item->transfer_out; ?></td>
                            <td><?php echo $item->trans_out_amt; ?></td>
                            <td><?php echo $item->adjustment; ?></td>
                            <td><?php echo $item->adjustment_amt; ?></td>
                            <td><?php echo $item->bets; ?></td>
                            <td><?php echo $item->bets_amount; ?></td>
                            <td><?php echo $item->bets_valid_amt; ?></td>
                            <td><?php echo $item->bets_win_loss; ?></td>
                            <td><?php echo $item->bets_comm; ?></td>
                            <td><?php echo $item->bets_bonus_amt; ?></td>
                            <td><?php echo $item->bets_adj; ?></td>
                            <td><?php echo $item->bets_adj_amt; ?></td>
                            <td><?php echo $item->bets_total_win_loss; ?></td>
                            <td><?php echo $item->bets_jackpot; ?></td>
                            <td><?php echo date("Y, M d g:i:A", strtotime($item->date_upload)); ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>

            </table>
        <?php
        } else {
            echo '<div class="bg-danger text-center"><p style="padding: 10px;font-size: 20px;">No Record Found!</p></div>';
        }
        ?>
        <div class="loading" style="display: none;">
            <div class="content"><img src="<?php echo base_url() . 'assets/img/loading.gif'; ?>" /></div>
        </div>
        <div class="col-md-12">
            <div style="overflow: auto; margin-top: 30px;">
                <?php echo $pagination; ?>
            </div>

        </div>
    </div>
</div>
<script>
    var sendRequest = function() {
        var searchKey = $('#member').val();
        var from = $('#search_fromdate').val();
        var to = $('#search_todate').val();
        var limitRows = $('#limitRows').val();
        window.location.href = '<?= base_url('sms/searchMember/') ?>?query=' + searchKey + '&from=' + from + '&to=' + to + ' &orderField=' + curOrderField + '&orderDirection=' + curOrderDirection + '&limitRows=' + limitRows;
        $('.loading').show();
    }


    var getNamedParameter = function(key) {
        if (key == undefined) return false;

        var url = window.location.href;
        //console.log(url);
        var path_arr = url.split('?');
        if (path_arr.length === 1) {
            return null;
        }
        path_arr = path_arr[1].split('&');
        path_arr = remove_value(path_arr, "");
        var value = undefined;
        for (var i = 0; i < path_arr.length; i++) {
            var keyValue = path_arr[i].split('=');
            if (keyValue[0] == key) {
                value = keyValue[1];
                break;
            }
        }

        return value;
    };


    var remove_value = function(value, remove) {
        if (value.indexOf(remove) > -1) {
            value.splice(value.indexOf(remove), 1);
            remove_value(value, remove);
        }
        return value;
    };


    var curOrderField, curOrderDirection;
    $('[data-action="sort"]').on('click', function(e) {
        curOrderField = $(this).data('title');
        curOrderDirection = $(this).data('direction');
        sendRequest();
    });
    $('#member').val(decodeURIComponent(getNamedParameter('query') || ""));
    $('#search_fromdate').val(decodeURIComponent(getNamedParameter('from') || ""));
    $('#search_todate').val(decodeURIComponent(getNamedParameter('to') || ""));
    $('#limitRows option[value="' + getNamedParameter('limitRows') + '"]').attr('selected', true);

    var curOrderField = getNamedParameter('orderField') || "";
    var curOrderDirection = getNamedParameter('orderDirection') || "";
    var currentSort = $('[data-action="sort"][data-title="' + getNamedParameter('orderField') + '"]');
    if (curOrderDirection == "ASC") {
        currentSort.attr('data-direction', "DESC").find('i.fa').removeClass('fa-sort-amount-asc').addClass('fa-sort-amount-desc');
    } else {
        currentSort.attr('data-direction', "ASC").find('i.fa').removeClass('fa-sort-amount-desc').addClass('fa-sort-amount-asc');
    }
</script>