<div id="drophere" class="card">
  <div class="card-header">
    <h3 class="card-title">Format : CSV / TXT</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
        <i class="fas fa-minus"></i></button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
        <i class="fas fa-times"></i></button>
    </div>
  </div>
  <div class="card-body">
    <div id="actions" class="row">

      <div class="col-lg-7">

        <div>
          <div class="form-group counter">
            <label>Upload title (optional)</label>
            <textarea class="form-control filedescription" rows="3" placeholder="upload title"></textarea>
          </div>
        </div>
        <!-- The fileinput-button span is used to style the file input field as button -->
        <div id="dragarea" class=" fileinput-button dz-clickable" style="border: 1px dashed;border-width: 3px;border-color: #636567;height: 150px;text-align: center;padding: 50px;">
          Drag the file here or click in the area.
        </div>
      </div>

      <div class="col-lg-5">
        <!-- The global file processing state -->
        <span style="display:none" class="fileupload-process">
          <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" style="opacity: 0;">
            <div class="progress-bar progress-bar-success" style="width: 100%;" data-dz-uploadprogress=""></div>
          </div>
        </span>
      </div>

    </div>

  </div>
  <div class="card-body">
    <div class="table table-striped" class="files" id="previews">
      <div id="template" class="file-row">
        <!-- This is used as the file preview template -->
        <div>
          <span class="preview"><img data-dz-thumbnail /></span>
        </div>
        <!-- <div style="float: right;display: none;" class="file-succes">
          <div class="alert alert-success" role="alert">提交成功:
            <span>0</span>条
          </div>
        </div> -->
        <div>
          <p class="name" data-dz-name></p>
          <strong class="error text-danger" data-dz-errormessage></strong>
        </div>
        <div>
          <p class="size" data-dz-size></p>
          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
          </div>
        </div>
        <div class="options-actions">
          <button class="btn btn-primary start">
            <i class="glyphicon glyphicon-upload"></i>
            <span>开始提交</span>
          </button>
          <button data-dz-remove class="btn btn-warning cancel">
            <i class="glyphicon glyphicon-ban-circle"></i>
            <span>取消</span>
          </button>
          <!-- <button data-dz-remove class="btn btn-danger delete">
            <i class="glyphicon glyphicon-trash"></i>
            <span>Delete</span>
          </button> -->
        </div>
      </div>
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
  </div>
  <!-- /.card-footer-->
</div>




<script>
  $(document).ready(function() {
    $('body').on("keyup", "textarea.filedescription", function() {
      let trimval = ($(this).val()).substring(0, 60);
      $(this).val(trimval);
      $(".counter .char-count").html((60 - $(this).val().length));
    });

    // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
    var previewNode = document.querySelector("#template");
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    var myDropzone = new Dropzone('#drophere', {
      url: "<?php echo base_url(); ?>sms/uploadFile", // Set the url
      maxFiles: 1,
      thumbnailWidth: 80,
      thumbnailHeight: 80,
      parallelUploads: 20,
      previewTemplate: previewTemplate,
      autoQueue: false, // Make sure the files aren't queued until manually added
      previewsContainer: "#previews", // Define the container to display the previews
      clickable: ".dz-clickable" // Define the element that should be used as click trigger to select files.
    });

    myDropzone.on("addedfile", function(file) {
      // Hookup the start button
      file.previewElement.querySelector(".start").onclick = function() {
        myDropzone.enqueueFile(file);
      };
    });

    // // Update the total progress bar
    myDropzone.on("totaluploadprogress", function(progress) {
      document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
    });

    myDropzone.on("sending", function(file, xhr, formData) {
      //ADDITIONAL DATA
      $description = $(".filedescription").val();
      formData.append('content', $description);
      // Show the total progress bar when upload starts
      document.querySelector("#total-progress").style.opacity = "1";
      // And disable the start button
      file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
      $(file.previewElement).find(".start").html('uploading...')
    });

    myDropzone.on("error", function(file, errormessage, xhr) {
      if (xhr) {
        //toastr.error(xhr.responseText);
        Swal.fire({
          icon: 'error',
          title: "" + xhr.responseText + "",
          showConfirmButton: false,
          timer: 2200
        })
        $(file.previewElement).find(".start").html('error').removeClass('btn-primary').addClass('btn-danger');
        myDropzone.files = [];
      }
    });

    myDropzone.on("maxfilesexceeded", function(file) {
      toastr.error('Exceed the maximum number of files, only one file is allowed !');
      myDropzone.removeFile(file);
    });

    // Hide the total progress bar when nothing's uploading anymore
    myDropzone.on("queuecomplete", function(progress) {
      document.querySelector("#total-progress").style.opacity = "0";

    });

    myDropzone.on("success", function(data) {
      var parsedData = JSON.parse(data.xhr.response);
      var uploadElement = data.previewElement;
      // $(uploadElement).find('.file-succes .alert-success span').html(parsedData.linecount);
      // toastr.success('提交成功:' + parsedData.linecount + '条');
      Swal.fire({
        icon: "success",
        title: "Submitted successfully : " + parsedData.linecount + "",
        showConfirmButton: false,
        timer: 2200
      });

      $(uploadElement).find('.file-succes').fadeIn();
      $(data.previewElement).find(".start").html('done').removeClass('btn-primary').addClass('btn-success');
      $(data.previewElement).find('.progress-bar-success').css('background', '#28a745');
      myDropzone.files = [];
    });


    // Setup the buttons for all transfers
    // The "add files" button doesn't need to be setup because the config
    // `clickable` has already been specified.
    // document.querySelector("#actions .start").onclick = function() {
    //   myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
    // };
    // document.querySelector("#actions .cancel").onclick = function() {
    //   myDropzone.removeAllFiles(true);
    // };
  })
</script>