<?php $this->load->view('component/htmlheader'); ?>

<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<!-- Navbar -->
		<?php $this->load->view('component/nav'); ?>
		<!-- /.navbar -->
		<!-- Main Sidebar Container -->
		<?php
		$id = $this->session->userdata('user_id');
		$data['rolename'] = $this->db->query('SELECT role.name, user.username FROM user LEFT JOIN role ON role.id = user.role_id WHERE user.id = "' . $id . '"')->row_array();
		$data['username'] = $this->db->query('SELECT username FROM user WHERE id ="' . $id . '"')->row_array();
		$this->load->view('component/sidebar', $data);
		?>
		<!-- Main End Sidebar Container -->
		<div class="content-wrapper" style="min-height: 1589.56px;">
			<br>
			<section class="content">
				<?php echo $subview; ?>
			</section>
		</div>
		<?php $this->load->view('component/footer'); ?>

	</div>
	<?php $this->load->view('component/htmlfooter'); ?>