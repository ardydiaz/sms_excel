<?php
defined('BASEPATH') or exit('No direct script access allowed');
session_write_close();
class Sms extends CI_Controller
{

	private $response = array(
		'success' => '',
		'message' => '',
		'errorCode' => '',
		'data' => array()
	);
	public function __construct()
	{
		Parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->database();
		$this->load->library('PHPexcel');
		$this->load->library('excel');
		/*model*/
		$this->load->model('sms_model');
		$this->load->model('user_model');
		$this->load->model('search_member_model');
		if ((!$this->session->userdata('islogin'))) {
			redirect(site_url() . "auth", 'refresh');
		}
	}

	private function sendResponse()
	{
		if ($this->response['errorCode']) {
			header('HTTP/1.1 ' . $this->response['errorCode'] . ' Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
		} else {
			$this->response['success'] = true;
		}
		echo json_encode($this->response);
	}

	public function index()
	{
		$data['title'] = "Excel";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/sms', $data, TRUE);
		$this->load->view('layout', $data);
	}

	public function import()
	{
		$data['title'] = "File Upload";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/import', $data, TRUE);
		$this->load->view('layout', $data);
	}

	public function saveImport()
	{

		if (isset($_FILES['file'])) {
			$file = $_FILES['file'];
			$allowedtypes = array('application/vnd.ms-excel', 'text/plain', 'text/csv', 'text/xlsx', 'text/tsv', 'application/csv', 'text/comma-separated-values', 'application/excel');
			if (in_array($_FILES["file"]["type"], $allowedtypes)) {
				if ($file['type'] == "text/csv") { //USE EXCEL READER IF FILE IS EXCEL
					$path = $_FILES["file"]["tmp_name"];
					$object = PHPExcel_IOFactory::load($path);
					foreach ($object->getWorksheetIterator() as $worksheet) {
						$highestRow = $worksheet->getHighestRow();
						$highestColumn = $worksheet->getHighestColumn();
						for ($row = 2; $row <= $highestRow; $row++) {
							$group = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
							$merchant_id = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
							$member_group = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
							$login_id = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
							$player_id = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
							$currency = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
							$transfer_in = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
							$trans_in_amt = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
							$transfer_out = $worksheet->getCellByColumnAndRow(8, $row)->getValue();
							$trans_out_amt = $worksheet->getCellByColumnAndRow(9, $row)->getValue();
							$adjustment = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
							$adjustment_amt = $worksheet->getCellByColumnAndRow(11, $row)->getValue();
							$bets = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
							$bets_amount = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
							$bets_valid_amt = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
							$bets_win_loss = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
							$bets_comm = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
							$bets_bonus_amt = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
							$bets_adj = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
							$bets_adj_amt = $worksheet->getCellByColumnAndRow(19, $row)->getValue();
							$bets_total_win_loss = $worksheet->getCellByColumnAndRow(20, $row)->getValue();
							$bets_jackpot = $worksheet->getCellByColumnAndRow(21, $row)->getValue();

							$checkMember = $this->getMember($login_id, $member_group, $merchant_id);
							if (empty($checkMember)) {
								// echo '<pre>';
								// print_r($checkMember);
								$data = array(
									'name' => $this->removeSpecialChar($login_id),
									'class' => $this->removeSpecialChar($member_group),
									'group_id' => $this->removeSpecialChar($merchant_id),
								);
								$this->db->insert('member', $data);
								$recordId = $this->db->insert_id();
								$data2 = array(
									'group' => $this->removeSpecialChar($group),
									'merchant_id' => $this->removeSpecialChar($merchant_id),
									'member_group' => $this->removeSpecialChar($member_group),
									'member_id' => $recordId,
									'login_id' => $this->removeSpecialChar($login_id),
									'player_id' => $this->removeSpecialChar($player_id),
									'currency' => $this->removeSpecialChar($currency),
									'transfer_in' => $transfer_in,
									'trans_in_amt' => $trans_in_amt,
									'transfer_out' => $transfer_out,
									'trans_out_amt' => $trans_out_amt,
									'adjustment' => $adjustment,
									'adjustment_amt' => $adjustment_amt,
									'bets' => $bets,
									'bets_amount' => $bets_amount,
									'bets_valid_amt' => $bets_valid_amt,
									'bets_win_loss' => $bets_win_loss,
									'bets_comm' => $bets_comm,
									'bets_bonus_amt' => $bets_bonus_amt,
									'bets_adj' => $bets_adj,
									'bets_adj_amt' => $bets_adj_amt,
									'bets_total_win_loss' => $bets_total_win_loss,
									'bets_jackpot' => $bets_jackpot,
									'date_upload' => date('Y-m-d H:i:s'),
									'created_at' => date('Y-m-d'),
								);
								$this->db->insert('excel_files', $data2);
							} else {
								$data = array(
									'name' => $this->removeSpecialChar($login_id),
									'class' => $this->removeSpecialChar($member_group),
									'group_id' => $this->removeSpecialChar($merchant_id),
								);
								$check = $this->sms_model->getMember($data);
								$data = array();
								foreach ($check as $key => $value) {
									$data[$value['id']] = $value;
								}
								foreach ($data as $item) {
									$data3 = array(
										'group' => $this->removeSpecialChar($group),
										'merchant_id' => $this->removeSpecialChar($merchant_id),
										'member_group' => $this->removeSpecialChar($member_group),
										'member_id' => $item['id'],
										'login_id' => $this->removeSpecialChar($login_id),
										'player_id' => $this->removeSpecialChar($player_id),
										'currency' => $this->removeSpecialChar($currency),
										'transfer_in' => $transfer_in,
										'trans_in_amt' => $trans_in_amt,
										'transfer_out' => $transfer_out,
										'trans_out_amt' => $trans_out_amt,
										'adjustment' => $adjustment,
										'adjustment_amt' => $adjustment_amt,
										'bets' => $bets,
										'bets_amount' => $bets_amount,
										'bets_valid_amt' => $bets_valid_amt,
										'bets_win_loss' => $bets_win_loss,
										'bets_comm' => $bets_comm,
										'bets_bonus_amt' => $bets_bonus_amt,
										'bets_adj' => $bets_adj,
										'bets_adj_amt' => $bets_adj_amt,
										'bets_total_win_loss' => $bets_total_win_loss,
										'bets_jackpot' => $bets_jackpot,
										'date_upload' => date('Y-m-d H:i:s'),
										'created_at' => date('Y-m-d'),
									);
									$this->db->insert('excel_files', $data3);
								}
							}
						}
					}
				}
			} else {
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
				echo 'Please upload a legal file format !';
			}
		}
	}


	public function getMember($login_id, $member_group, $merchant_id)
	{
		$array = array();
		$data = array(
			'name' => $this->removeSpecialChar($login_id),
			'class' => $this->removeSpecialChar($member_group),
			'group_id' => $this->removeSpecialChar($merchant_id),
		);
		$this->db->select('*')->from('member')->where($data);
		$get = $this->db->get();

		if (!empty($get)) {
			foreach ($get->result() as $row) {
				$array[] = $row->name;
				$array[] = $row->class;
				$array[] = $row->group_id;
			}
		}
		return $array;
	}
	public function getExcelResult()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$query = $this->sms_model->showExcel($from, $to);
		$result = array();
		$data = array();
		$ar = array();
		foreach ($query as $values) {
			if (!isset($data[$values['created_at']])) {
				$data[$values['created_at']] = $values;
			}
		}
		krsort($data);
		if (sizeof($data) == 0) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
			echo 'No result found !';
		} else {
			foreach ($data as $item) {
				$date = array();
				$date['created_at'] = $item['created_at'];
				$ar[] = $date;
			}
			$result['content'] = $ar;
		}
		echo json_encode($result);
	}

	public function showExcelDate($created_at = null)
	{
		if ($created_at) {
			$config['base_url'] = site_url("sms/showExcelDate/$created_at");

			$config['per_page'] = ($this->input->get('limitRows')) ? $this->input->get('limitRows') : 20;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = TRUE;
			$config['reuse_query_string'] = TRUE;

			$config['full_tag_open'] = '<ul class="pagination">';
			$config['full_tag_close'] = '</ul>';
			$config['attributes'] = ['class' => 'page-link'];

			$config['first_tag_open'] = '<li class="page-item">';
			$config['first_tag_close'] = '</li>';
			$config['prev_link'] = 'Previous';
			$config['prev_tag_open'] = '<li class="page-item">';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = 'Next';
			$config['next_tag_open'] = '<li class="page-item">';
			$config['next_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li class="page-item">';
			$config['last_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="page-item active"><a href="' . $config['base_url'] . '?per_page=0" class="page-link">';
			$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
			$config['num_tag_open'] = '<li class="page-item">';
			$config['num_tag_close'] = '</li>';



			$data['page'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
			$data['orderField'] = ($this->input->get('orderField')) ? $this->input->get('orderField') : '';
			$data['orderDirection'] = ($this->input->get('orderDirection')) ? $this->input->get('orderDirection') : '';


			$data['list'] = $this->sms_model->getData($created_at, $config["per_page"], $data['page'],  $data['orderField'], $data['orderDirection']);
			$config['total_rows'] = $this->sms_model->count_date_excel($created_at, $config["per_page"], $data['page'],  $data['orderField'], $data['orderDirection']);
			$data['date'] = $created_at;
			$this->pagination->initialize($config);
			$data['pagination'] = $this->pagination->create_links();
			$data['role'] = $this->session->userdata('name'); //get user role
			$data['subview'] = $this->load->view('sms/excel_by_date', $data, TRUE);
			$this->load->view('layout', $data);
		}
	}


	public function searchMember()
	{

		//pagination settings
		$config['base_url'] = site_url("sms/searchMember/");
		$config['per_page'] = ($this->input->get('limitRows')) ? $this->input->get('limitRows') : 20;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['reuse_query_string'] = TRUE;



		// integrate bootstrap pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = ['class' => 'page-link'];
		$config['first_link'] = TRUE;
		$config['last_link'] = TRUE;
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="' . $config['base_url'] . '?per_page=0" class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';

		$data['page'] = ($this->input->get('per_page')) ? $this->input->get('per_page') : 0;
		$data['searchFor'] = ($this->input->get('query')) ? $this->input->get('query') : NULL;
		$data['from'] = ($this->input->get('from')) ? $this->input->get('from') : NULL;
		$data['to'] = ($this->input->get('to')) ? $this->input->get('to') : NULL;
		$data['orderField'] = ($this->input->get('orderField')) ? $this->input->get('orderField') : 'bets_valid_amt';
		$data['orderDirection'] = ($this->input->get('orderDirection')) ? $this->input->get('orderDirection') : 'DESC';


		// get excel list
		$data['item_list'] = $this->sms_model->get_excel_pagi($config["per_page"], $data['page'], $data['searchFor'], $data['from'], $data['to'], $data['orderField'], $data['orderDirection']);
		$config['total_rows'] = $this->sms_model->count_all_excel($data['searchFor'], $data['from'], $data['to'], $data['orderField'], $data['orderDirection']);

		// echo '<pre>';
		// print_r($config['total_rows']);
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();

		// load view
		$data['title'] = "Search by Member";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/search_member', $data, TRUE);
		$this->load->view('layout', $data);
	}

	public function search_by($sort_by = 'bets_valid_amt', $sort_order = 'DESC')
	{

		// get search string
		$memberName = ($this->input->post("member")) ? $this->input->post("member") : "NIL";
		$from = ($this->input->post("from")) ? $this->input->post("from") : "NIL";
		$to = ($this->input->post("to")) ? $this->input->post("to") : "NIL";

		$memberName = ($this->uri->segment(3)) ? $this->uri->segment(3) : $memberName;
		$from = ($this->uri->segment(3)) ? $this->uri->segment(3) : $from;
		$to = ($this->uri->segment(3)) ? $this->uri->segment(3) : $to;

		// pagination settings
		$config = array();
		$config['base_url'] = site_url("sms/search_by/$memberName/$from/$to");
		$config['total_rows'] = $this->sms_model->search_count($sort_by, $sort_order, $memberName, $from, $to);
		$config['per_page'] = "15";
		// integrate bootstrap pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['attributes'] = ['class' => 'page-link'];
		$config['first_link'] = TRUE;
		$config['last_link'] = TRUE;
		$config['first_tag_open'] = '<li class="page-item">';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="page-item">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li class="page-item">';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li class="page-item">';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
		$config['num_tag_open'] = '<li class="page-item">';
		$config['num_tag_close'] = '</li>';
		if (ceil($config['total_rows'] / $config['per_page']) > 5) {
			$config['last_link'] = '.... Last';
		} else {
			$config['last_link'] = 'Last';
		}
		$this->pagination->initialize($config);
		$data['pagination'] = $this->pagination->create_links();
		$data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		// get excel list
		$data['item_list'] = $this->sms_model->search_excel($config["per_page"], $data['page'], $from, $to, $memberName, $sort_by, $sort_order,);
		$data['sort_by'] = $sort_by; //bets_valid_amt
		$data['sort_order'] = $sort_order; //DESC

		// load view
		$data['title'] = "Search by Member";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('sms/search_member', $data, TRUE);
		$this->load->view('layout', $data);
	}


	public function removeSpecialChar($str)
	{
		$res = preg_replace('/[\@\=\.\;\" "]+/', '', $str);
		return $res;
	}

	public function seed_excel()
	{
		$nameArr = array();
		for ($d = 900000; count($nameArr) < $d;) {
			$randomString = $this->random_char(2, 5);
			if (!in_array($randomString, $nameArr, true)) {
				array_push($nameArr,  $randomString);
				$data = array(
					'group' => rand(1, 5),
					'merchant_id' => '109',
					'member_group' => $randomString,
					'member_id' => $randomString,
					'login_id' => $randomString,
					'player_id' => rand(1, 1000),
					'currency' => 'IDR',
					'transfer_in' => rand(1, 1000),
					'trans_in_amt' => rand(1, 1000),
					'transfer_out' => rand(1, 1000),
					'trans_out_amt' => rand(1, 1000),
					'adjustment' => rand(1, 1000),
					'adjustment_amt' => rand(1, 1000),
					'bets' => rand(1, 1000),
					'bets_amount' => rand(1, 1000),
					'bets_valid_amt' => rand(1, 1000),
					'bets_win_loss' => rand(1, 1000),
					'bets_comm' => rand(1, 1000),
					'bets_bonus_amt' => rand(1, 1000),
					'bets_adj' => rand(1, 1000),
					'bets_adj_amt' => rand(1, 1000),
					'bets_total_win_loss' => rand(1, 1000),
					'bets_jackpot' => rand(1, 1000),
					'date_upload' => date('Y-m-d H:i:s'),
					'created_at' => date('Y-m-d'),
				);
				$this->sms_model->insert_data($data);
			}
		}
		echo '<h1>900000 user records added to database </h1></br>';
		print_r($nameArr);
	}

	private function random_char($minlen = 0, $maxlen = 6)
	{
		$characters = explode(" ", 'a b c d e f g h i j k l m n o p q r s t u ');
		$charactersLength = count($characters);
		$randloop = rand($minlen, $maxlen);
		$charc = array();
		for ($i = 0; $i < $randloop; $i++) {
			$charc[] = $characters[rand(0, $charactersLength - 1)];
		}
		return join('', $charc);
	}
}
