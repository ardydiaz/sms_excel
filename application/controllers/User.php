<?php
defined('BASEPATH') or exit('No direct script access allowed');
session_write_close();
class User extends CI_Controller
{
	private $response = array(
		'success' => '',
		'message' => '',
		'errorCode' => '',
		'data' => array()
	);
	public function __construct()
	{
		Parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->database();
		/*model*/
		$this->load->model('user_model');
		// if ((!$this->session->userdata('islogin'))) {
		// 	redirect(site_url() . "auth", 'refresh');
		// }

		$isLoggedIn = $this->session->userdata('islogin');
		if (!isset($isLoggedIn) || $isLoggedIn != TRUE) {
			redirect('auth');
		}
	}


	private function sendResponse()
	{
		if ($this->response['errorCode']) {
			header('HTTP/1.1 ' . $this->response['errorCode'] . ' Internal Server');
			header('Content-Type: application/json; charset=UTF-8');
		} else {
			$this->response['success'] = true;
		}
		echo json_encode($this->response);
	}


	public function index()
	{
		$data['title'] = "Users";
		$data['role'] = $this->session->userdata('name'); //get user role
		$data['subview'] = $this->load->view('user/add_account', $data, TRUE);
		$this->load->view('layout', $data);
	}

	public function addUsers()
	{
		$fullname = $this->input->post('fullname');
		$password =  md5($this->input->post('password'));
		$username = $this->input->post('username');
		$role = $this->input->post('role');
		if (empty($fullname) || empty($password) || empty($username) || empty($role)) {
			$this->response['message'] = ' Required Fields !';
			$this->response['errorCode'] = 406;
			$this->response['success'] = false;
		} else {
			$data = array(
				'fullname' => $fullname,
				'password' => $password,
				'username' => $username,
				'role_id' => $role,
				'created_at' => date('Y-m-d H:i:s'),
			);
			$this->db->insert('user', $data);
			$this->response['message'] = 'Successfully added user !';
			$this->response['success'] = true;
		}
		$this->sendResponse();
	}

	public function getUsers()
	{
		$order_static = array('fullname', 'username', 'role', 'created_at');
		$orderby = array($order_static[$this->input->get("order")[0]["column"]], $this->input->get("order")[0]["dir"]);
		$sorting = join(' ', $orderby);
		$offset = $this->input->get("length");
		$start = $this->input->get("start");
		$searchpara = $this->input->get("search")['value'];
		$resdata = $this->db->query("SELECT u.*, r.name
		FROM user u
		INNER JOIN `role` r ON r.id = u.role_id
		WHERE (u.fullname LIKE '%" . $searchpara . "%' 
		OR u.username LIKE '%" . $searchpara . "%' )  
		ORDER BY " . $sorting . "   LIMIT " . $start . ", " . $offset . "")->result();
		$data = array();
		$data["recordsFiltered"] =  $this->db->query("SELECT * FROM user ")->num_rows();
		$data['data'] = array();
		foreach ($resdata as $value) {
			$data['data'][] = array(
				$value->fullname,
				$value->username,
				$value->name ?  '<span class="badge badge-success">' . $value->name . '</span>' : '',
				date("D, d M g:i:A", strtotime($value->created_at)),
			);
		}
		$data["recordsTotal"] = count($data['data']);
		echo json_encode($data);
	}

	public function logout()
	{
		$this->session->sess_destroy(); //destroy the session
		redirect(site_url() . 'auth/');
	}
}
