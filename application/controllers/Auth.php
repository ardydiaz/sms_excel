<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		Parent::__construct();
		$this->load->library('session');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->library('user_agent');
		$this->load->database();
		/*model*/
		$this->load->model('auth_model');
	}

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'password', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('auth/login');
		} else {
			$data = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'last_login_ip' => $this->input->ip_address(),
				'last_login_date' => date('Y-m-d H:i:s'),
				'status' => 0,
				'islogin' => TRUE,
			);
			$query = $this->auth_model->user_login($data);
			if ($query) {
				$data['user_id'] = $query->id;
				$data['role'] = $query->role_id;
				$data['name'] = $query->role_id;
				$data['username'] = $query->username;
				$this->session->set_userdata($data);
				$this->db->where('id', $query->id);
				$this->db->update(
					'user',
					array(
						'login_ip' => $this->input->ip_address(),
						'last_login_ip' => $query->login_ip,
						'last_login_date' => date('Y-m-d H:i:s'),
					)
				); //store date and time into database

				$this->session->set_flashdata('ok', 'Successfully logged in');
				redirect(site_url() . 'user/');
			} else {
				$this->session->set_flashdata('error', 'Invalid username/password');
				$this->load->view('auth/login', $data);
			}
		}
	}
}
