<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Sms_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }



  public function showExcel($from, $to)
  {
    $sql = "SELECT created_at FROM excel_files FORCE INDEX(idx_member, idx_created)
             WHERE created_at IN (SELECT DISTINCT created_at FROM excel_files WHERE created_at BETWEEN '" . $from . "' AND '" . $to . "'  ORDER BY bets_valid_amt DESC)";
    return ($this->db->query($sql)->result_array());
  }



  public function getMember($data)
  {
    $this->db->select('*');
    $this->db->from('member');
    $this->db->where($data);
    $get = $this->db->get();
    return $get->result_array();
  }

  public function getData($created_at, $limit, $start,  $orderField, $orderDirection)
  {

    $this->db->select('e.*, m.name as memberName');
    $this->db->from('excel_files e FORCE INDEX(idx_member, idx_created)');
    $this->db->join('member m', 'm.id = e.member_id', 'left');
    $this->db->like('e.created_at', $created_at, 'both');
    $this->db->limit($limit, $start);
    $this->db->order_by($orderField, $orderDirection);
    $query = $this->db->get();
    return $query->result();
  }


  public function count_date_excel($created_at, $orderField, $orderDirection)
  {
    $this->db->select('e.*, m.name as memberName');
    $this->db->from('excel_files e FORCE INDEX(idx_member, idx_created)');
    $this->db->join('member m', 'm.id = e.member_id', 'left');
    $this->db->like('e.created_at', $created_at, 'both');
    $this->db->order_by($orderField, $orderDirection);
    $query = $this->db->get();

    return $query->num_rows();
  }


  public function get_excel_pagi($limit, $start, $st = "", $from = "", $to = "", $orderField, $orderDirection)
  {
    $sql = "SELECT e.*, m.name as memberName
    FROM (excel_files e FORCE INDEX(idx_member, idx_created)) 
    LEFT JOIN member m ON m.id = e.member_id 
    WHERE (e.created_at BETWEEN '" . $from . "'  AND '" . $to . "' ) OR (m.name LIKE '%" . $st . "%')
    ORDER BY " . $orderField . " " . $orderDirection . "
    LIMIT " . $start . ", " . $limit . "";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public function count_all_excel($st = "", $from = "", $to = "", $orderField, $orderDirection)
  {

    $sql = "SELECT e.*, m.name as memberName
    FROM (excel_files e FORCE INDEX(idx_member, idx_created)) 
    LEFT JOIN member m ON m.id = e.member_id 
    WHERE (created_at BETWEEN '" . $from . "' AND '" . $to . "') OR (m.name LIKE '%" . $st . "%') ORDER BY " . $orderField . " " . $orderDirection . " ";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function search_excel($sort_by, $sort_order, $from, $to, $memberName)
  {
    $sort_order = ($sort_order == 'DESC') ? 'DESC' : 'ASC';
    $sort_columns = array(
      'group',
      'merchant_id', 'member_group', 'member_id',
      'player_id', 'currency', 'transfer_in',
      'trans_in_amt', 'transfer_out', 'trans_out_amt',
      'adjustment', 'adjustment_amt', 'bets',
      'bets_amount', 'bets_valid_amt',
      'bets_win_loss', 'bets_comm',
      'bets_bonus_amt', 'bets_adj',
      'bets_adj_amt', 'bets_total_win_loss',
      'bets_jackpot', 'date_upload'
    );
    $sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'bets_valid_amt';

    $sql = "SELECT e.*, 
    m.name as memberName 
    FROM excel_files e FORCE INDEX(idx_member, idx_created)
    INNER JOIN member m ON m.id = e.member_id 
    WHERE created_at BETWEEN '%" . $from . "%' AND '%" . $to . "%' OR m.name like '%" . $memberName . "%'  ORDER BY " . $sort_by . " " . $sort_order;
    $query = $this->db->query($sql);
    return $query->result();
  }



  public function search_count($sort_by, $sort_order, $member, $from, $to)
  {
    $sql = "SELECT e.*, m.name as memberName FROM excel_files e FORCE INDEX(idx_member, idx_created)
    INNER JOIN member m ON m.id = e.member_id 
    WHERE created_at BETWEEN '%$from%' 
    AND '%$to%' OR m.name like '%$member%' ORDER BY " . $sort_by . " " . $sort_order . " ";

    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function insert_data($data)
  {
    $this->db->insert('excel_files', $data);
    return $this->db->insert_id();
  }
}
