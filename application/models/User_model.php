<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    public function getAccount()
    {
        $sql = "SELECT * FROM user ORDER BY id DESC";
        return ($this->db->query($sql)->result());
    }
}
