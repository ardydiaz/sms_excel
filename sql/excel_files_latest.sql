-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2022 at 03:35 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms_excel`
--

-- --------------------------------------------------------

--
-- Table structure for table `excel_files`
--

CREATE TABLE `excel_files` (
  `id` int(11) NOT NULL,
  `group` varchar(100) DEFAULT NULL,
  `merchant_id` varchar(100) DEFAULT NULL,
  `member_group` varchar(100) NOT NULL,
  `member_id` longtext NOT NULL,
  `login_id` varchar(100) NOT NULL,
  `player_id` varchar(100) NOT NULL,
  `currency` varchar(100) NOT NULL,
  `transfer_in` varchar(100) NOT NULL,
  `trans_in_amt` varchar(100) NOT NULL,
  `transfer_out` varchar(100) NOT NULL,
  `trans_out_amt` varchar(100) NOT NULL,
  `adjustment` varchar(100) NOT NULL,
  `adjustment_amt` varchar(100) NOT NULL,
  `bets` varchar(100) NOT NULL,
  `bets_amount` varchar(100) NOT NULL,
  `bets_valid_amt` varchar(100) NOT NULL,
  `bets_win_loss` varchar(100) NOT NULL,
  `bets_comm` varchar(100) NOT NULL,
  `bets_bonus_amt` varchar(100) NOT NULL,
  `bets_adj` varchar(100) NOT NULL,
  `bets_adj_amt` varchar(100) NOT NULL,
  `bets_total_win_loss` varchar(100) NOT NULL,
  `bets_jackpot` varchar(100) NOT NULL,
  `date_upload` varchar(100) NOT NULL,
  `created_at` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `excel_files`
--
ALTER TABLE `excel_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_member` (`member_id`(768)),
  ADD KEY `idx_created` (`created_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `excel_files`
--
ALTER TABLE `excel_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
