var methods = {
  addAdmin: function () {
    let data = {
      fullname: $.trim($("#fullname").val()),
      username: $.trim($("#username").val()),
      password: $.trim($("#password").val()),
      role: $.trim($("#role").val()),
    };
    apis.add_admin(data).then(
      function (data) {
        //reset field
        $("#fullname").val("");
        $("#username").val("");
        $("#password").val("");
        $("#role").val("");
        Swal.fire({
          icon: "success",
          text: "Successfully added user ! ",
        });
        listTable.ajax.reload();
        // setTimeout(function () {
        //   window.location.reload();
        // }, 1300);
      },
      function (data) {
        toastr.error(data.responseJSON.message);
      }
    );
  },
};
